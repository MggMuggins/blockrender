use std::ops::Mul;

macro_rules! mtrx3x3 {
    {[$a:expr, $b:expr, $c:expr]
    [$d:expr, $e:expr, $f:expr]
    [$g:expr, $h:expr, $i:expr]} => {
        Matrix3x3::new([$a, $b, $c, $d, $e, $f, $g, $h, $i])
    }
}

#[derive(Debug, PartialEq)]
struct Matrix3x3 {
    row_major: [f64; 9],
}

impl Matrix3x3 {
    fn new(row_major: [f64; 9]) -> Matrix3x3 {
        Matrix3x3 {
            row_major,
        }
    }
    
    /// Panics if indx is not 0, 1, or 2
    fn row(&self, indx: usize) -> [f64; 3] {
        let mut row = [0.0; 3];
        row.copy_from_slice(&self.row_major[indx*3..indx*3 + 3]);
        row
    }
    
    fn set_row(&mut self, indx: usize, row: [f64; 3]) {
        self.row_major[indx*3..indx*3 + 3].copy_from_slice(&row[..]);
    }
    
    fn col(&self, indx: usize) -> [f64; 3] {
        [self.row_major[indx], self.row_major[indx+3], self.row_major[indx+6]]
    }
    
    fn set_col(&mut self, indx: usize, col: [f64; 3]) {
        self.row_major[0 + indx] = col[0];
        self.row_major[3 + indx] = col[1];
        self.row_major[6 + indx] = col[2];
    }
}

impl Mul for Matrix3x3 {
    type Output = Self;
    
    fn mul(self, rhs: Self) -> Self {
        let mut rslt = Matrix3x3::new([0.0; 9]);
        
        for (indx, entry) in rslt.row_major.iter_mut().enumerate() {
            let row = self.row(indx / 3);
            let col = rhs.col(indx % 3);
            
            *entry = row.iter()
                .zip(col.iter())
                .map(|(row_e, col_e)| row_e * col_e )
                .sum();
        }
        rslt
    }
}

fn main() {
    let mtrx = mtrx3x3! {
        [1.0, 0.0, 3.0]
        [0.0, 1.0, 0.0]
        [0.0, 0.0, 1.0]
    };
    println!("{:?}", mtrx.row(0));
    println!("{:?}", mtrx.col(2));
}

#[cfg(test)]
mod test {
    use crate::Matrix3x3;
    
    #[test]
    fn test_row_col() {
        let mtrx = mtrx3x3! {
            [1.0, 0.0, 3.0]
            [0.0, 1.0, 0.0]
            [0.0, 0.0, 1.0]
        };
        assert_eq!(mtrx.row(0), [1.0, 0.0, 3.0]);
        assert_eq!(mtrx.row(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.row(2), [0.0, 0.0, 1.0]);
        
        assert_eq!(mtrx.col(0), [1.0, 0.0, 0.0]);
        assert_eq!(mtrx.col(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.col(2), [3.0, 0.0, 1.0]);
    }
    
    #[test]
    fn test_set_row() {
        let mut mtrx = Matrix3x3::new([0.0; 9]);
        
        mtrx.set_row(0, [1.0, 0.0, 3.0]);
        mtrx.set_row(1, [0.0, 1.0, 0.0]);
        mtrx.set_row(2, [0.0, 0.0, 1.0]);
        
        assert_eq!(mtrx.row(0), [1.0, 0.0, 3.0]);
        assert_eq!(mtrx.row(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.row(2), [0.0, 0.0, 1.0]);
        
        assert_eq!(mtrx.col(0), [1.0, 0.0, 0.0]);
        assert_eq!(mtrx.col(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.col(2), [3.0, 0.0, 1.0]);
    }
    
    #[test]
    fn test_set_col() {
        let mut mtrx = Matrix3x3::new([0.0; 9]);
        
        mtrx.set_col(0, [1.0, 0.0, 0.0]);
        mtrx.set_col(1, [0.0, 1.0, 0.0]);
        mtrx.set_col(2, [3.0, 0.0, 1.0]);
        
        assert_eq!(mtrx.row(0), [1.0, 0.0, 3.0]);
        assert_eq!(mtrx.row(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.row(2), [0.0, 0.0, 1.0]);
        
        assert_eq!(mtrx.col(0), [1.0, 0.0, 0.0]);
        assert_eq!(mtrx.col(1), [0.0, 1.0, 0.0]);
        assert_eq!(mtrx.col(2), [3.0, 0.0, 1.0]);
    }
    
    #[test]
    fn test_mul() {
        let m1 = mtrx3x3! {
            [1.0, 2.0, 3.0]
            [0.0, 1.0, 4.0]
            [-1.0, 5.0, 2.0]
        };
        let m2 = mtrx3x3! {
            [1.0, 2.0, 5.0]
            [-1.0, 0.0, 2.0]
            [0.0, 1.0, 1.0]
        };
        
        let rslt = mtrx3x3! {
            [-1.0, 5.0, 12.0]
            [-1.0, 4.0, 6.0]
            [-6.0, 0.0, 7.0]
        };
        
        assert_eq!(m1 * m2, rslt);
    }
}

