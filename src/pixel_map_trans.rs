use std::error::Error;
use std::f32::consts::FRAC_PI_2;
use std::fs::File;
use std::io::Read;

use nalgebra::{
    Matrix4,
    Rotation3,
    Translation3,
    Vector3,
    Vector4,
};
use png::Decoder;

use crate::consts::projection_matrix;

trait VecTransform {
    fn transform(&mut self, mtrx: Matrix4<f32>);
}

impl VecTransform for Vec<NotPixel> {
    fn transform(&mut self, mtrx: Matrix4<f32>) {
        for point in self.iter_mut() {
            point.transform(mtrx);
        }
    }
}

#[derive(Debug)]
struct NotPixel {
    x: f32,
    y: f32,
    z: f32,
    
    rgb: u32,
}

impl NotPixel {
    fn from_png(r: impl Read) -> Result<Vec<NotPixel>, Box<dyn Error>> {
        let (info, mut stream) = Decoder::new(r)
            .read_info()?;
        
        println!("Color type: {:?}", stream.output_color_type());
        println!("Height: {}\nWidth: {}", info.height, info.width);
        let mut pixels = Vec::with_capacity((info.width * info.height) as usize);
        
        for row in 0..info.height {
            let row_iter = stream.next_row()?
                .expect("Range 0..info.height incorrect")
                .chunks(3)
                .enumerate();
            
            for (col, rgb) in row_iter {
                // Read the image into the y-z plane
                pixels.push(NotPixel {
                    x: 0.0,
                    y: row as f32,
                    z: col as f32,
                    rgb: ((rgb[0] as u32) << 16)
                        | ((rgb[1] as u32) << 8)
                        | (rgb[2] as u32),
                });
            }
        }
        Ok(pixels)
    }
    
    fn transform(&mut self, mtrx: Matrix4<f32>) {
        let vec4 = Vector4::new(self.x, self.y, self.z, 1.0);
        let rslt = mtrx * vec4;
        self.x = rslt.x;
        self.y = rslt.y;
        self.z = rslt.z;
    }
}

#[allow(dead_code)]
pub fn run() -> Result<(), Box<dyn Error>> {
    let filename = "boneBlock.png";
    println!("Loading png file {}", filename);
    
    let left_file = File::open(&filename)?;
    let right_file = File::open(&filename)?;
    let top_file = File::open(&filename)?;
    let mut left = NotPixel::from_png(left_file)?;
    let mut right = NotPixel::from_png(right_file)?;
    let mut top = NotPixel::from_png(top_file)?;
    
    // Left is mapped into the y-z plane, so no need to change here
    
    // Top rotates around z and then translates positive y
    let top_rot = Vector3::z() * FRAC_PI_2;
    let top_trns = (Translation3::new(0.0, 16.0, 0.0) * Rotation3::new(top_rot))
        .to_matrix();
    top.transform(top_trns);
    
    // Right rotates around y and then translates positive z
    let right_rot = Vector3::y() * FRAC_PI_2;
    let right_trns = (Translation3::new(0.0, 0.0, 16.0) * Rotation3::new(right_rot))
        .to_matrix();
    right.transform(right_trns);
    
    let mut not_pixels = Vec::with_capacity(left.len() + right.len() + top.len());
    not_pixels.append(&mut left);
    not_pixels.append(&mut right);
    not_pixels.append(&mut top);
    
    not_pixels.transform(projection_matrix(1.0));
    println!("{:#x?}\n{}", not_pixels, not_pixels.len());
    
    Ok(())
}

