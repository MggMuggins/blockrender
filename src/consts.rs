use std::f32::consts::{FRAC_PI_4, FRAC_PI_6};

use nalgebra::{Matrix4, Vector4};

const Y_DEG: f32 = -FRAC_PI_4;

const X_DEG: f32 = FRAC_PI_6;

pub fn projection_matrix(scale_factor: f32) -> Matrix4<f32> {
    // Rotate to the right around the y-axis (when looking from +z)
    let rot_y: Matrix4<f32> = Matrix4::new(
        Y_DEG.cos(), 0.0, -Y_DEG.sin(), 0.0,
        0.0, 1.0, 0.0, 0.0,
        Y_DEG.sin(), 0.0, Y_DEG.cos(), 0.0,
        0.0, 0.0, 0.0, 1.0,
    );
    println!("Y-rotation matrix: {}", rot_y);
    
    // Push up a bit to keep the projection in the 1st quadrant
    /*
    let trans_y: Matrix4<f32> = Matrix4::new(
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.353,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0,
    );
    println!("Y-translation matrix: {}", trans_y);*/
    
    let rot_x: Matrix4<f32> = Matrix4::new(
        1.0, 0.0, 0.0, 0.0,
        0.0, X_DEG.cos(), -X_DEG.sin(), 0.0,
        0.0, X_DEG.sin(), X_DEG.cos(), 0.0,
        0.0, 0.0, 0.0, 1.0,
    );
    println!("X rotation matrix: {}", rot_x);
    
    let o_proj: Matrix4<f32> =
        Matrix4::from_diagonal(&Vector4::new(1.0, 1.0, 0.0, 1.0));
    println!("Ortho matrix: {}", o_proj);
    
    scale_factor * o_proj /* * trans_y*/ * rot_x * rot_y
}

