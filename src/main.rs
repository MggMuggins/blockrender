use std::process;

use nalgebra::Vector4;

mod consts;
mod mesh;
mod pixel_map_trans;

use consts::projection_matrix;

fn row_vec(v: &Vector4<f32>) -> String {
    format!("({}, {}, {})", v.x, v.y, v.z)
}

// Project the vertices of a cube through the transformation matrix
#[allow(dead_code)]
fn run() {
    let vertices = vec![
        // z=0 plane
        Vector4::new(0.0, 0.0, 0.0, 1.0),
        Vector4::new(1.0, 0.0, 0.0, 1.0),
        Vector4::new(1.0, 1.0, 0.0, 1.0),
        Vector4::new(0.0, 1.0, 0.0, 1.0),
        
        // z=1 plane
        Vector4::new(0.0, 0.0, 1.0, 1.0),
        Vector4::new(1.0, 0.0, 1.0, 1.0),
        Vector4::new(1.0, 1.0, 1.0, 1.0),
        Vector4::new(0.0, 1.0, 1.0, 1.0),
    ];
    
    let mut proj_vertices = Vec::with_capacity(vertices.len());
    
    println!("Dimetric matrix: {}", projection_matrix(1.0));
    
    for vertex in vertices {
        let projected = projection_matrix(1.0) * vertex;
        proj_vertices.push(projected);
    }
    
    for vert in proj_vertices.iter() {
        println!("{}", row_vec(vert));
    }
}

fn main() {
    mesh::run().unwrap_or_else(|e| {
            eprintln!("Error: {}", e);
            process::exit(1);
        });
}

