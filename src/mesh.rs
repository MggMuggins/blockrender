use std::cmp::Ordering;
use std::error::Error;
use std::fs::File;
use std::io::Read;

use array2d::Array2D;
use nalgebra::{Matrix4, Vector2, Vector4};
use png::{BitDepth, Decoder, Encoder, ColorType};

use crate::consts::projection_matrix;

pub trait Point {
    fn zero() -> Self;
}
/// 2D point
impl Point for Vector2<i32> {
    fn zero() -> Self {
        Vector2::zeros()
    }
}
/// 3D homogenous point
impl Point for Vector4<f32> {
    fn zero() -> Self {
        Vector4::zeros()
    }
}

/// A mesh is a list of triangular surfaces
pub struct Mesh<P: Point> {
    surfaces: Vec<Surface<P>>,
}

impl Mesh<Vector4<f32>> {
    pub fn project(&self, matrix: &Matrix4<f32>) -> Mesh<Vector2<i32>> {
        let mut rslt = Mesh {
            surfaces: Vec::with_capacity(self.surfaces.len()),
        };
        for surface in self.surfaces.iter() {
            rslt.surfaces.push(surface.project(matrix));
        }
        rslt
    }
}

impl Mesh<Vector2<i32>> {
    fn bounds(&self) -> (i32, i32, i32, i32) {
        let mut iterator = self.surfaces.iter()
            .flatten();
        let first = iterator.next()
            .expect("No vertices when measuring bounds");
        
        let (mut max_x, mut min_x) = (first.x, first.x);
        let (mut max_y, mut min_y) = (first.y, first.y);
        
        for vertex in iterator {
            if vertex.x > max_x {
                max_x = vertex.x;
            }
            
            if vertex.x < min_x {
                min_x = vertex.x;
            }
            
            if vertex.y > max_y {
                max_y = vertex.y;
            }
            
            if vertex.y < min_y {
                min_y = vertex.y;
            }
        }
        (max_x, min_x, max_y, min_y)
    }
    
    fn corresponding_surface(&self, point: Vector2<i32>) -> Option<Surface<Vector2<i32>>> {
        for surface in self.surfaces.iter() {
            if surface.contains(point) {
                return Some(surface.clone());
            }
        }
        None
    }
}

/// Find the slope of the line bounded by these points
fn slope(first: Vector2<i32>, second: Vector2<i32>) -> Option<f32> {
    let delta_x = (second.x - first.x) as f32;
    let delta_y = (second.y - first.y) as f32;
    
    if delta_x == 0.0 {
        None
    } else {
        Some(delta_y / delta_x)
    }
}

/// This is very abstract... it _should_ return true when either parameter is
/// Ordering::Equal, or if the orderings are the same
fn on_same_side(first: Option<Ordering>, second: Option<Ordering>) -> bool {
    let (first, second) = if let (Some(first), Some(second)) = (first, second){
        (first, second)
    } else {
        return false;
    };
    
    if first == Ordering::Equal || second == Ordering::Equal {
        true
    } else {
        first == second
    }
}

fn barycentric(p: Vector2<f32>, vertices: &[Vector2<f32>; 3]) -> [f32; 3] {
    /* Using cross product to do area ratios (BROKEN)
    let [a, b, c] = vertices;
    
    let ab = b - a;
    
    let ac = c - a;
    let cb = b - c;
    let ba = a - b;
    
    let area_abc = 0.5 * ab.cross(&ac).norm();
    
    let ax = x - a;
    let bx = x - b;
    let cx = x - c;
    
    let area_axc = 0.5 * ax.cross(&ac).norm();
    let area_cxb = 0.5 * cx.cross(&cb).norm();
    let area_bxa = 0.5 * bx.cross(&ba).norm();
    
    return [area_cxb/area_abc, area_axc/area_abc, area_bxa/area_abc];
    */
    // These equations are derived from
    // p = a(alpha) + b(beta) + c(gamma)
    // alpha = 1 - beta - gamma
    /*
    let [a, b, c] = vertices;
    let beta = ((a.y * c.x) + (p.x * c.y) - (a.x * c.y) - (p.y * c.x))
        / ((b.x * c.y) - (b.y * c.x));
    let gamma = (p.x - a.x - (beta * b.x)) / (c.x);
    let alpha = 1.0 - beta - gamma;
    return [alpha, beta, gamma];
    */
    // Copied directly from
    // https://observablehq.com/@infowantstobeseen/barycentric-coordinates
    let [a, b, c] = vertices;
    let ab = b - a;
    let ac = c - a;
    let ap = p - a;
    
    let nac = Vector2::new(a.y - c.y, c.x - a.x);
    let nab = Vector2::new(a.y - b.y, b.x - a.x);
    
    let beta = ap.dot(&nac) / ab.dot(&nac);
    let gamma = ap.dot(&nab) / ac.dot(&nab);
    let alpha = 1.0 - beta - gamma;
    return [alpha, beta, gamma];
}

/// A triangle
#[derive(Clone, Debug)]
pub struct Surface<P: Point> {
    vertices: [Vertex<P>; 3],
    color: [u8; 4],
}

impl<P: Point> Surface<P> {
    pub fn new(first: Vertex<P>, second: Vertex<P>, third: Vertex<P>) -> Surface<P> {
        Surface {
            vertices: [
                first,
                second,
                third,
            ],
            color: [rand::random(), rand::random(), rand::random(), 255],
        }
    }
}

impl Surface<Vector4<f32>> {
    pub fn project(&self, matrix: &Matrix4<f32>) -> Surface<Vector2<i32>> {
        Surface {
            vertices:[
                self.vertices[0].project(matrix),
                self.vertices[1].project(matrix),
                self.vertices[2].project(matrix),
            ],
            color: [rand::random(), rand::random(), rand::random(), 255],
        }
    }
}

impl Surface<Vector2<i32>> {
    /// Determine if a point (from the final raster image) is found in this 2D
    /// surface.
    /*
    pub fn contains(&self, point: Vector2<i32>) -> bool {
        if self.vertices[0].point == point
            || self.vertices[1].point == point
            || self.vertices[2].point == point
        {
            return true;
        }
        
        for third_vertex_indx in 0..3 {
            let first = self.vertices[(third_vertex_indx + 1) % 3].point;
            let second = self.vertices[(third_vertex_indx + 2) % 3].point;
            let third = self.vertices[third_vertex_indx].point;
            
            if let Some(edge_slope) = slope(first, second) {
                // Point-slope form gives corresponding y-value for a given x
                // on the line formed by the `first` and `second` vertices
                let corresp_y_for_point =
                    (edge_slope * ((point.x - first.x) as f32)) + first.y as f32;
                let corresp_y_for_third =
                    (edge_slope * ((third.x - first.x) as f32)) + first.y as f32;
                
                if ! on_same_side(corresp_y_for_point.partial_cmp(&(point.y as f32)),
                    corresp_y_for_third.partial_cmp(&(third.y as f32)))
                {
                    return false;
                }
            } else { // The slope was undefined (vertical line)
                if ! on_same_side(point.x.partial_cmp(&first.x),
                    third.x.partial_cmp(&first.x))
                {
                    return false;
                }
            }
        }
        return true;
    }*/
    
    pub fn vertices(&self) -> [Vector2<f32>; 3] {
        let verts: Vec<_> = self.vertices.iter()
            .map(|vertex| Vector2::new(vertex.point.x as f32, vertex.point.y as f32) )
            .collect();
        [verts[0], verts[1], verts[2]]
    }
    
    pub fn tex_coords(&self) -> [Vector2<f32>; 3] {
        let verts: Vec<_> = self.vertices.iter()
            .map(|vertex| Vector2::new(vertex.texture_coord.x as f32, vertex.texture_coord.y as f32) )
            .collect();
        [verts[0], verts[1], verts[2]]
    }
    
    pub fn contains(&self, point: Vector2<i32>) -> bool {
        let point = Vector2::new(point.x as f32, point.y as f32);
        
        let bary = barycentric(point, &self.vertices());
        
        for b_coord in bary.iter() {
            if *b_coord < 0.0 {
                return false;
            }
        }
        return true;
    }
    
    /// Get the color for a given point inside this surface (RGB)
    pub fn color(&self, point: Vector2<i32>, texture: &Texture) -> [u8; 4] {
        let point = Vector2::new(point.x as f32, point.y as f32);
        
        let [alpha, beta, gamma] = barycentric(point, &self.vertices());
        let [a, b, c] = self.tex_coords();
        
        let dest_point = (a * alpha) + (b * beta) + (c * gamma);
        let dest_point = Vector2::new(
            (dest_point.x.round() as i32),
            (dest_point.y.round() as i32),
        );
        texture.color_at(dest_point)
    }
}

impl<P: Point + Clone> IntoIterator for &Surface<P> {
    type Item = P;
    type IntoIter = SurfaceIter<P>;
    
    fn into_iter(self) -> Self::IntoIter {
        SurfaceIter {
            vertices: [
                self.vertices[0].clone(),
                self.vertices[1].clone(),
                self.vertices[2].clone(),
            ],
            cur: 0,
        }
    }
}

pub struct SurfaceIter<P: Point> {
    vertices: [Vertex<P>; 3],
    cur: usize,
}

impl<P: Point + Clone> Iterator for SurfaceIter<P> {
    type Item = P;
    
    fn next(&mut self) -> Option<P> {
        if self.cur >= 3 {
            None
        } else {
            let rslt = self.vertices[self.cur].point.clone();
            self.cur += 1;
            Some(rslt)
        }
    }
}

#[derive(Clone, Debug)]
pub struct Vertex<P: Point> {
    point: P,
    texture_coord: Vector2<u16>,
}

impl<P: Point> Vertex<P> {
    pub fn new(point: P, texture_coord: Vector2<u16>) -> Vertex<P> {
        Vertex {
            point,
            texture_coord,
        }
    }
}

impl Vertex<Vector4<f32>> {
    pub fn project(&self, matrix: &Matrix4<f32>) -> Vertex<Vector2<i32>> {
        let proj_point = matrix * self.point;
        Vertex {
            point: Vector2::new(proj_point.x.round() as i32, proj_point.y.round() as i32),
            texture_coord: self.texture_coord,
        }
    }
}

/// 2D array of rgba pixels
pub struct Texture {
    data: Array2D<[u8; 4]>,
}

impl Texture {
    //TODO: Read bit-depth and color type from png file
    fn from_png(r: impl Read) -> Result<Texture, Box<dyn Error>> {
        let (info, mut stream) = Decoder::new(r)
            .read_info()?;
        
        println!("Color type: {:?}", stream.output_color_type());
        println!("Height: {}\nWidth: {}", info.height, info.width);
        let mut rows = Vec::with_capacity(info.height as usize);
        
        for _ in 0..info.height {
            let row: Vec<_> = stream.next_row()?
                .expect("Range 0..info.height incorrect")
                .chunks(3)
                .map(|rgb| [rgb[0], rgb[1], rgb[2], 255] )
                .collect();
            rows.push(row);
        }
        Ok(Texture {
            data: Array2D::from_rows(&rows),
        })
    }
    
    // Texture coords have (0,0) in the lower left, instead of the upper left
    fn color_at(&self, tex_coord: Vector2<i32>) -> [u8; 4] {
        // therefore we invert y
        let x = tex_coord.x as usize;
        let y = self.data.column_len() - (tex_coord.y as usize);
        // Failed gets just return transparency
        *self.data.get(x, y)
            .unwrap_or_else(|| {
                println!("WARN: Tried to get ({}, {}) from texture", x, y);
                &[0, 0, 0, 0]
            })
            //.expect(&format!("tried to get ({}, {}), from texture", x, y))
    }
}

pub fn run() -> Result<(), Box<dyn Error>> {
    let mesh = Mesh {
        surfaces: vec![
            // x = 0 plane
            Surface::new(
                Vertex::new(
                    Vector4::new(0.0, 0.0, 0.0, 1.0),
                    // Texture coordinates have the origin at the upper left
                    Vector2::new(0, 15),
                ),
                Vertex::new(
                    Vector4::new(0.0, 0.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 0.0, 1.0),
                    Vector2::new(0, 0),
                ),
            ),
            Surface::new(
                Vertex::new(
                    Vector4::new(0.0, 1.0, 0.0, 1.0),
                    Vector2::new(0, 0),
                ),
                Vertex::new(
                    Vector4::new(0.0, 0.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 1.0, 1.0),
                    Vector2::new(15, 0),
                ),
            ),
            // z = 1 plane
            Surface::new(
                Vertex::new(
                    Vector4::new(0.0, 0.0, 1.0, 1.0),
                    Vector2::new(0, 15),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 1.0, 1.0),
                    Vector2::new(0, 0),
                ),
                Vertex::new(
                    Vector4::new(1.0, 0.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
            ),
            Surface::new(
                Vertex::new(
                    Vector4::new(1.0, 1.0, 1.0, 1.0),
                    Vector2::new(15, 0),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 1.0, 1.0),
                    Vector2::new(0, 0),
                ),
                Vertex::new(
                    Vector4::new(1.0, 0.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
            ),
            // y = 1 plane
            Surface::new(
                Vertex::new(
                    Vector4::new(0.0, 1.0, 0.0, 1.0),
                    Vector2::new(0, 0),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 1.0, 1.0),
                    Vector2::new(0, 15),
                ),
                Vertex::new(
                    Vector4::new(1.0, 1.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
            ),
            Surface::new(
                Vertex::new(
                    Vector4::new(1.0, 1.0, 0.0, 1.0),
                    Vector2::new(15, 0),
                ),
                Vertex::new(
                    Vector4::new(0.0, 1.0, 0.0, 1.0),
                    Vector2::new(0, 0),
                ),
                Vertex::new(
                    Vector4::new(1.0, 1.0, 1.0, 1.0),
                    Vector2::new(15, 15),
                ),
            ),
        ]
    };
    
    // Small scale factor so the vertex pixels are meaningfully close to each
    // other
    let proj_matrix = projection_matrix(800.0);
    println!("Projection matrix: {}", proj_matrix);
    
    let projected = mesh.project(&proj_matrix);
    
    println!("Bounds: {:?}", projected.bounds());
    let (max_x, min_x, max_y, min_y) = projected.bounds();
    let (width, height) = (max_x - min_x + 1, max_y - min_y + 1);
    
    let mut texture_file = File::open("alloyForge_front.png")?;
    let texture = Texture::from_png(&mut texture_file)?;
    let mut out_color_data = Vec::with_capacity((width * height) as usize);
    
    // Reverse the y-axis, otherwise the entire thing is upside-down
    for y in (min_y..max_y + 1).rev() {
        for x in min_x..max_x + 1 {
            let point = Vector2::new(x, y);
            
            
            if let Some(cor_surface) = projected.corresponding_surface(point) {
                let color = cor_surface.color(point, &texture);
                out_color_data.extend_from_slice(&color);
            } else {
                // Fully transparent black
                out_color_data.extend_from_slice(&[0, 0, 0, 0]);
            }
        }
    }
    
    let mut encoder = Encoder::new(File::create("out.png")?, width as u32, height as u32);
    encoder.set_color(ColorType::RGBA);
    encoder.set_depth(BitDepth::Eight);
    
    let mut writer = encoder.write_header()?;
    writer.write_image_data(&out_color_data)?;
    Ok(())
}

#[cfg(test)]
mod test {
    use nalgebra::Vector2;
    
    use crate::mesh::barycentric;
    
    #[test]
    fn test_barycentric() {
        let p = Vector2::new(0.5, 0.5);
        let vertices = [
            Vector2::new(0.0, 0.0),
            Vector2::new(0.0, 1.0),
            Vector2::new(1.0, 0.0),
        ];
        
        assert_eq!([0.0, 0.5, 0.5], barycentric(p, &vertices));
        
        let p = Vector2::new(1.0, 1.0);
        assert_eq!([-1.0, 1.0, 1.0], barycentric(p, &vertices));
        
        // Make sure that our bary's are 1 at each point
        let p = vertices[0];
        assert_eq!([1.0, 0.0, 0.0], barycentric(p, &vertices));
        let p = vertices[1];
        assert_eq!([0.0, 1.0, 0.0], barycentric(p, &vertices));
        let p = vertices[2];
        assert_eq!([0.0, 0.0, 1.0], barycentric(p, &vertices));
        /*
        let p = Vector2::new(0.0, 0.0);
        let vertices = [
            Vector2::new(0.0, 1.0),
            Vector2::new(1.0, -1.0),
            Vector2::new(-1.0, -1.0),
        ];
        assert_eq!([1.0/3.0, 1.0/3.0, 1.0/3.0], barycentric(p, &vertices));
        */
    }
}
