# blockrender
This repository is a collection of experiments that I've made over several
months regarding the linear algebra behind 3D rendering. The code is a massive
mess, there are several parts that do not all play together at all. The most
interesting stuff is in `mesh.rs` and `consts.rs`, nothing else is really worth
looking at.

This also was used as the basis for a paper for MATH270 in the Spring of 2021.

